import { ref, computed } from "vue";
import { defineStore } from "pinia";

export const useLoginStore = defineStore("login", () => {
  const loginName = ref("");
  const isLogin = computed(() => {
    return loginName.value !== "";
  });

  const login = (userName: string) => {
    loginName.value = userName;
    localStorage.setItem("LoginName", userName);
  };

  const logout = () => {
    loginName.value = "";
    localStorage.removeItem("LoginName");
  };

  const loadData = () => {
    loginName.value = localStorage.getItem("LoginName") || "";
  };

  return { loginName, isLogin, login, logout, loadData };
});
